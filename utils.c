/* Some useful functions and macros.
 * Author: lukjp
 */

#include <stdint.h>
#include "utils.h"

int32_t addSatInt32(int32_t a, int32_t b) {
    int64_t sum = (((int64_t) a) + b);
    if (sum > INT32_MAX) {
        sum = INT32_MAX;
    }
    if (sum < INT32_MIN) {
        sum = INT32_MIN;
    }
    return (int32_t) sum;
}

uint8_t clip32Bit(int32_t *val, int32_t max) {
    if (*val > max) {
        *val = max;
        return 1;
    } else if (*val < -max) {
        *val = -max;
        return 1;
    } else {
        return 0;
    }
}

int32_t nearVal(int32_t oldVal, int32_t target, int32_t step) {
    int32_t newVal;
    if (target > oldVal) {
        newVal = addSatInt32(oldVal,
                step);
        if (newVal > target) {
            // we are at the end of acceleration
            newVal = target;
        }
    } else {
        // target < oldVal
        newVal = addSatInt32(oldVal,
                -step);
        if (newVal < target) {
            // we are at the end of acceleration
            newVal = target;
        }
    }
    return newVal;
}
