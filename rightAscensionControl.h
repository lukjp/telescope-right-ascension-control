/* rightAscensionControl:
 * Main control of the right ascension of a telescope
 * It is configured by config.h
 * Author: lukjp
 */

#ifndef RIGHTASCENSIONCONTROL_H_
#define RIGHTASCENSIONCONTROL_H_

/* Initialize the control.
 * To be called once.
 */
void rightAscensionControlInit(void);

/* Update state of motor
 * Must be called if a switch changed state
 */
void rightAscensionControlLoop(void);

#endif /* RIGHTASCENSIONCONTROL_H_ */
