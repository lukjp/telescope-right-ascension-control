/* Simple control of the right ascension of an telescope with an Atmel AVR 
 * microcontroller and a stepper motor. See README.md for more information.
 * Author: lukjp
 */

#include <stdint.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include "PWMout.h"
#include "rightAscensionControl.h"

int main(void) {
    // change clock prescaler to 1, to allow usage with default fuses
    CLKPR = (1 << 7);
    CLKPR = 0;
    rightAscensionControlInit();
    sei();
    pwmOutInit();

    while (1) {
        rightAscensionControlLoop();
    }
}
