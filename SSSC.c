/* SSSC: Simple Single Stepper Controller
 * Author: lukjp
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include "SSSC.h"
#include "stepper.h"
#include "utils.h"

static struct StepperConfig stepperConfig;
static struct Stepper stepper;

static void setupRecalculationTimer(void);
static void setupStepTimer(void);
static void enableStepTimerInterrupt(void);
static void disableStepTimerInterrupt(void);

// array of pin states of the stepper
#if (STEPPER_USE_HALF_STEPS)
static const uint8_t StepperPortStateBitMask[STEPPER_STATE_COUNT] = {
        (1 << STEPPER_PIN_0), (1 << STEPPER_PIN_0) | (1 << STEPPER_PIN_1),
        (1 << STEPPER_PIN_1), (1 << STEPPER_PIN_1) | (1 << STEPPER_PIN_2),
        (1 << STEPPER_PIN_2), (1 << STEPPER_PIN_2) | (1 << STEPPER_PIN_3),
        (1 << STEPPER_PIN_3), (1 << STEPPER_PIN_3) | (1 << STEPPER_PIN_0) };
#else
static const uint8_t StepperPortStateBitMask[STEPPER_STATE_COUNT] =
{   (1 << STEPPER_PIN_0), (1 << STEPPER_PIN_1), (1 << STEPPER_PIN_2),
    (1 << STEPPER_PIN_3)};
#endif

void ssscSetup(void) {
    if ( STEPPER_USE_HALF_STEPS) {
        stepperCreateConfig(&stepperConfig, (STEPPER_MAX_SPEED * 2),
                (STEPPER_MAX_ACCEL * 2), RECALC_TIME);
    } else {
        stepperCreateConfig(&stepperConfig, STEPPER_MAX_SPEED,
        STEPPER_MAX_ACCEL, RECALC_TIME);
    }
    stepperInit(&stepperConfig, &stepper, &STEPPER_PORT, STEPPER_USED_BIT_MASK,
    STEPPER_OFF_BIT_MASK, STEPPER_STATE_COUNT, StepperPortStateBitMask);
    STEPPER_DDR |= STEPPER_USED_BIT_MASK; // enable output of pins after init
    setupRecalculationTimer();
    setupStepTimer();
}

void ssscSetSpeed(int32_t speed) {
    if (STEPPER_USE_HALF_STEPS) {
        speed *= 2;
    }
    if (STEPPER_AUTO_DISABLE) {
        if (speed != 0) {
            stepperSetSpeed(&stepper, speed);
            stepperStart(&stepper);
        } else {
            stepperDisable(&stepper);
        }
    } else {
        stepperSetSpeed(&stepper, speed);
    }
}

void ssscSetTicsPerStep(uint32_t ticsPerStep, uint8_t backwards) {
    if (STEPPER_USE_HALF_STEPS) {
        ticsPerStep /= 2;
    }
    if (STEPPER_AUTO_DISABLE) {
        if (ticsPerStep != 0) {
            stepperSetTicsPerStep(&stepper, ticsPerStep, backwards);
            stepperStart(&stepper);
        } else {
            stepperDisable(&stepper);
        }
    } else {
        stepperSetTicsPerStep(&stepper, ticsPerStep, backwards);
    }
}

ISR (TIMER2_OVF_vect) { // Recalculation timer
    struct StepperStepTime recalculationResult;
    recalculationResult = stepperRecalc(&stepper);
    switch (recalculationResult.stepTimeMode) {
    case noStep:
        disableStepTimerInterrupt();
        break;
    case updateStepTime:
        ;
        // divide by 64 (step timer prescaler) to get timer ticks
        uint32_t stepTime = DIV_ROUND_U_U(recalculationResult.stepTime, 64);
        // limit step time to maximum supported by 16 bit timer
        if (stepTime > UINT16_MAX) {
            stepTime = UINT16_MAX;
        }
        OCR1A = stepTime;
        // enable step timer overflow interrupt if disabled when the stepper
        // was stopped
        enableStepTimerInterrupt();
        break;
    case keepStepTime:
        break;
    }
}

ISR (TIMER1_OVF_vect) {
    stepperStep(&stepper);
}

static void setupRecalculationTimer(void) {
    // Setup recalculation timer: clock/256, output compare disconnected and
    // normal mode. Enable recalculation timer overflow interrupt.
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
    TCCR2A = 0;
    TCCR2B = (1 << CS22);
    TIMSK2 = (1 << TOIE2);
#elif defined  (__AVR_ATmega8__ )
    TCCR2 = (1 << CS22);
    TIMSK |= (1 << TOIE2);
#endif
}

static void setupStepTimer(void) {
    // Setup step Timer: fast pwm mode with OCR1A as top, output compare
    // disconnected and clock/64
    // In this mode we can change the frequency at any time, without danger of
    // missing a compare match an generating a glitch.
    // We do not enable interrupts, because the motor is still standing.
    TCCR1A = (1 << WGM11) | (1 << WGM10);
    TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS10) | (1 << CS11);
    if ( STEPPER_AUTO_DISABLE) {
        stepperStart(&stepper);
    }
}

static void enableStepTimerInterrupt(void) {
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
    TIMSK1 |= (1 << TOIE1);
#elif defined  (__AVR_ATmega8__ )
    TIMSK |= (1 << TOIE1);
#endif
}

static void disableStepTimerInterrupt(void) {
#if defined ( __AVR_ATmega328P__ ) || defined ( __AVR_ATmega328__ )
    TIMSK1 &= ~(1 << TOIE1);
#elif defined  (__AVR_ATmega8__ )
    TIMSK &= ~(1 << TOIE1);
#endif
}

