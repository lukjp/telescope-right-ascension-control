/*
 * Small and simple library for handling an unipolar stepper motor.
 * Currently only jog mode is supported, which means constant velocity
 * Terminology:
 *  - ticks: CPU-Ticks
 *  - state: which coil(s) are currently active
 *  - step: one coil forward (ex.: from A1 to B1)
 * Architecture:
 *  At start a configuration has to be created with StepperCreateConfig, then a
 *  stepper must be initialized with an config. Now the target speed can be
 *  controlled by StepperSetSpeed.
 *  For speed control StepperRecalc must be called regularly to accelerate.
 *  It returns what should be done and the time between the steps. If the
 *  stepper is running, StepperStep need then be called with this interval.
 * 
 * TODO:
 *  - stop the stepper with deceleration to 0 speed
 *  - start the stepper at 0 speed
 * Author: lukjp
 */

#include <stdint.h>

#ifndef STEPPER_H_
#define STEPPER_H_

enum StepperMode { // enum for future extendability
    running, // running with constant speed
    accelerating, // accelerating
    disabled // stepper disabled
};

struct StepperConfig { // configuration for stepper motor, can be shared between more
    uint32_t maxSpeed; // maximum speed in steps per 1000 seconds
    uint32_t maxSpeedDifference; // maximum by which the speed can increase or decrease per recalc
};

struct Stepper { // data for each stepper, unique to each stepper
    volatile int32_t setSpeed; // target speed for the motor in steps per 1000 seconds
    volatile uint32_t setTicsPerStep; // current speed in in steps per 1000 seconds
    volatile int32_t currentSpeed; // current speed in in steps per 1000 seconds
    volatile uint8_t state; // currently active coil
    volatile enum StepperMode mode; // current mode of the stepper
    volatile uint8_t * port; // address of the port where the stepper is connected
    uint8_t usedBitMask; // mask of used bits for stepper control, 1 for bits used by the stepper, 0 for unrelated bits
    uint8_t offBitMask; // bitmask when the stepper is turned off
    uint8_t stateCount; // number of different states for the stepper
    const uint8_t * stateBitMask; // pointer to array of port output masks for each state, array must be stateCount long
    struct StepperConfig * config; // configuration to use
};

enum StepTimeMode { // what should be done to the step timer
    noStep, // stepper is stopped, StepperStep need not be called
    keepStepTime, // keep step time, nothing need to be done
    updateStepTime // update the step time
};

struct StepperStepTime {
    enum StepTimeMode stepTimeMode; // what should be done
    uint32_t stepTime; // time between Steps in ticks if changed
};

/* Create a new config for a stepper motor
 * Parameters:
 *  - *config: Config to create
 *  - maxSpeed: maximum speed in steps per 1000 seconds
 *  - maxAcceleration: maximum acceleration in (steps per second) per second
 *  - recalculationInterval: time between calls of the recalc Function in ticks
 * return 0: stepper config could be created
 * return 1: one or more parameters are not possible
 */
uint8_t stepperCreateConfig(struct StepperConfig *config, uint32_t maxSpeed,
        uint32_t maxAcceleration, uint32_t recalculationInterval);

/* initialize a new stepper
 * Parameters:
 *  - *config : address of the config to use by the new stepper
 *  - *stepper: address of the data of the new stepper
 *  - *port: address of the PORT to use
 *  - usedBitMask: mask of used bits for stepper control, 1 for bits used by
 *                 the stepper, 0 for unrelated bits
 *  - offBitMask: bitmask for the stepper output when the stepper is turned off
 *  - stateCount: number of different states for the stepper
 *  - *stateBitMask: pointer to array of port output masks for each state,
 *                   array must be stateCount long
 * NOTE: pins not used by the stepper must be set to 0 in offBitMask and
 * stateBitMask
 * return 0: stepper initialized properly
 * return 1: one or more parameters are not possible
 */
uint8_t stepperInit(struct StepperConfig *config, struct Stepper *stepper,
        volatile uint8_t *port, uint8_t usedBitMask, uint8_t offBitMask,
        uint8_t stateCount, const uint8_t * stateBitMask);

/* Disable the stepper (turn off all coils)
 * Parameters:
 *  - *stepper: address of the data of the stepper
 * return 0: disabled stepper
 * return 1: stepper was already disabled
 */
uint8_t stepperDisable(struct Stepper *stepper);

/* Start the stepper, which means enable it, but assume it is running at 0 speed.
 * The stepper will start at standstill and accelerate to the setSpeed.
 * If the stepper was already running the function does nothing
 * Parameters:
 *  - *stepper: address of the data of the stepper
 * return 0: started stepper
 * return 1: stepper was already running
 */
uint8_t stepperStart(struct Stepper *stepper);

/* Set  target speed of the motor.
 * Parameters:
 *  - *stepper: address of the data of the stepper
 *  - speed: new speed for the motor in steps per 1000 seconds
 * return 0: speed set
 * return 1: absolute speed above maximum speed, speed set to maximum
 */
uint8_t stepperSetSpeed(struct Stepper *stepper, int32_t speed);

/* Set  target speed of the motor.
 * Parameters:
 *  - *stepper: address of the data of the stepper
 *  - ticsPerStep: new speed for the motor in tics per Step for the stepper
 *  - backwards: non-zero to set motor running backwards
 * return 0: speed set
 * return 1: absolute speed above maximum speed, speed set to maximum
 */
uint8_t stepperSetTicsPerStep(struct Stepper *stepper, uint32_t ticsPerStep,
        uint8_t backwards);

/* Recalculate time between Steps
 * Parameters:
 *  - *stepper: address of the data of the stepper
 * return value: stepperStepTime, see above
 */
struct StepperStepTime stepperRecalc(struct Stepper *stepper);

/* Step one step forward or backward if speed is not zero
 *  -*stepper: address of the data of the stepper
 */
void stepperStep(struct Stepper *stepper);

#endif // STEPPER_H_
