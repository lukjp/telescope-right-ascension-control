/* Configuration switches for the right ascension control.
 * Author: lukjp
 */

#include <stdint.h>

#ifndef CONFIG_H_
#define CONFIG_H_

// NOTE: The following constants can be changed

#define TEST_SPEED_PORT PORTC
#define TEST_SPEED_PIN PINC
#define TEST_SPEED_PIN_NR 3

#define DIRECTION_SWITCH_PORT PORTC
#define DIRECTION_SWITCH_PIN PINC
#define DIRECTION_SWITCH_PIN_NR 4

#define FOLLOW_SWITCH_PORT PORTC
#define FOLLOW_SWITCH_PIN PINC
#define FOLLOW_SWITCH_PIN_NR 5

#define MOON_SWITCH_PORT PORTC
#define MOON_SWITCH_PIN PINC
#define MOON_SWITCH_PIN_NR 2

#define OVERRIDE_ENABLE_PORT PORTC
#define OVERRIDE_ENABLE_PIN PINC
#define OVERRIDE_ENABLE_PIN_NR 1

#define OVERRIDE_POT_ADC_PIN 0

#define GEAR_TRANSMISSION_RATIO (140UL * 56)
// Step count for the stepper to revolve one time (excluding half steps)
#define STEPPER_STEP_COUNT 2048

// override speed for the motor in steps per 1000 seconds at full speed
// NOTE: This is the speed for maximum control input, if the potentiometer is
// calibrated at half of the supply voltage, this speed is not achievable.
#define OVERRIDE_MAX_SPEED 1000000

// NOTE: The following constants should not be changed

#define SIDEREAL_DAY_s 86164
// Time for the moon to appear on almost the same spot as the day before
// approximately 24 hours and 50 minutes.
// This value is not accurate, but i could not find a better value and i am too
// lazy to compute it myself.
#define MOON_APPARENT_ORBIT_PERIOD_s (60 * (60UL * 24 + 50))

#define STEPS_PER_REV ((uint64_t) GEAR_TRANSMISSION_RATIO * STEPPER_STEP_COUNT)

// Stepper speed to track the stars or the moon in steps per 1000 seconds
#define STARS_STEPPER_SPEED ((uint32_t) DIV_ROUND_U_U(1000 * STEPS_PER_REV, \
        SIDEREAL_DAY_s))
#define MOON_STEPPER_SPEED ((uint32_t) DIV_ROUND_U_U(1000 * STEPS_PER_REV, \
        MOON_APPARENT_ORBIT_PERIOD_s))

#endif // CONFIG_H_
