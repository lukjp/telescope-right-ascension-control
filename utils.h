/* Some useful functions and macros.
 * Author: lukjp
 */

#include <stdint.h>

#ifndef UTILS_H_
#define UTILS_H_

// divide with rounding instead of truncating
// divide signed by unsigned
#define DIV_ROUND_S_U(n, d) (((n) < 0) ? (((n) - (d) / 2) / (d)) : (((n) + (d) / 2) / (d)))
// divide unsigned by unsigned
#define DIV_ROUND_U_U(n, d) (((n) + (d) / 2) / (d))

// add with saturation (set overflows to max value)
int32_t addSatInt32(int32_t a, int32_t b);

// clip val between -max and +max
// return 1: clipped
// return 0: not clipped
uint8_t clip32Bit(int32_t *val, int32_t max);

// change oldVal by step to target
int32_t nearVal(int32_t oldVal, int32_t target, int32_t step);

#endif /* UTILS_H_ */
