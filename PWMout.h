/* Simple PWM output to reduce stepper power consumption
 * Author: lukjp
 */

#ifndef PWM_OUT_H_
#define PWM_OUT_H_

/* NOTE: The following constants can be changed */

// 56% duty cycle
#define PWM_OUT_TIMER_TOP 39
#define PWM_OUT_TIMER_COMP 24

/* NOTE: The following constants should not be changed*/
#define PWM_OUT_DDR DDRD
#define PWM_OUT_PIN_NR 5

void pwmOutInit(void);

#endif /* PWM_OUT_H_ */
