/* Small library for Handling the ADC of some AVR microcontrollers
 * 
 * Currently supported:
 * ATmega2560
 * ATmega328
 * ATmega328p
 * ATtiny13
 * 
 * Return code 0 means success.
 * 
 * There are two supported modes: 8 and 10 bit: If 8 bit mode is set in the
 * init call, the left adjusted mode in the adc is activated. Every call special
 * for 8 bit is assuming this is enabled.
 * Author: lukjp
 */

#include <stdint.h>

#ifndef ADC_H_
#define ADC_H_

#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) || defined (__AVR_ATmega2560__) || defined (__AVR_ATtiny13__) // supported devices

// initialize the adc and turn it on
uint8_t adcInit(uint8_t prescaler, uint8_t eightBitMode, uint8_t refVoltage,
        uint8_t autotrigger, uint8_t interruptEnable);

uint8_t adcSetAutoTriggerSource(uint8_t source);

uint8_t adcSetChannel(uint8_t channel);

// Starts a measurement and waits for the result
// If there is already one running, it aborts
// Works only in 8 bit mode
uint8_t adcMeasure8Bit(uint8_t *result);

// Starts a measurement and waits for the result
// If there is already one running, it aborts
// Works only in 10 bit mode
uint8_t adcMeasure10Bit(uint16_t *result);

// Measure n times and sum the results
// If a measurement is running, it will wait until it is finished
// n must not be greater than 64
// WARNING: will deadlock if ADC is in free running mode
uint8_t adcMeasure10BitNSum(uint16_t *result, uint8_t n);

// starts a new measurement
// errors if one is already running
uint8_t adcStartConversion(void);

// Reads the last result in 8 bit mode
uint8_t adcRead8Bit(void);

// Reads the last result in 10 bit mode
uint16_t adcRead10Bit(void);

// Checks if adc measurement is ongoing, returns one if the ADC is in use
uint8_t adcInUse(void);

// turn on adc if it was turned off
void adcEnable(void);

// turn off ADC
void adcDisable(void);

// ADC Prescaler
#define ADC_PRESCALER_2 0
#define ADC_PRESCALER_4 2
#define ADC_PRESCALER_8 3
#define ADC_PRESCALER_16 4
#define ADC_PRESCALER_32 5
#define ADC_PRESCALER_64 6
#define ADC_PRESCALER_128 7

#define ADC_NOT_LEFT_ADJUSTED 0
#define ADC_LEFT_ADJUSTED 1

#define ADC_AUTOTRIGGER_DISABLED 0
#define ADC_AUTOTRIGGER_ENABLED 1

#define ADC_INTERRUPT_DISABLED 0
#define ADC_INTERRUPT_ENABLED 1

#else
#error "device not supported"
#endif

#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) || defined (__AVR_ATmega2560__)

#define ADC_SOURCE_FREE_RUNNING 0
#define ADC_SOURCE_ANALOG_COMPERATOR 1
#define ADC_SOURCE_EXTERNAL_INT_0 2
#define ADC_SOURCE_TIM0_COMPA 3
#define ADC_SOURCE_TIM0_OVF 4
#define ADC_SOURCE_TIM1_COMPB 5
#define ADC_SOURCE_TIM1_OVF 6
#define ADC_SOURCE_TIM1_CAPT 7

#endif /* defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) || defined (__AVR_ATmega2560__) */

#if defined (__AVR_ATtiny13__)

#define ADC_SOURCE_FREE_RUNNING 0
#define ADC_SOURCE_ANALOG_COMPERATOR 1
#define ADC_SOURCE_EXTERNAL_INT_0 2
#define ADC_SOURCE_TIM_COMPA 3
#define ADC_SOURCE_TIM_OVF 4
#define ADC_SOURCE_TIM_COMPB 5
#define ADC_SOURCE_PCINT 6

#endif /* defined (__AVR_ATtiny13__) */

#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__)

// ADC Reference Voltages
#define ADC_REF_VOLTAGE_AREF 0
#define ADC_REF_VOLTAGE_AVCC 1
#define ADC_REF_VOLTAGE_1V1 3

// ADC Channel
#define  ADC_CHANNEL_SINGLE_0       0
#define  ADC_CHANNEL_SINGLE_1       1
#define  ADC_CHANNEL_SINGLE_2       2
#define  ADC_CHANNEL_SINGLE_3       3
#define  ADC_CHANNEL_SINGLE_4       4
#define  ADC_CHANNEL_SINGLE_5       5
#define  ADC_CHANNEL_SINGLE_6       6
#define  ADC_CHANNEL_SINGLE_7       7
#define  ADC_CHANNEL_SINGLE_8       8
#define  ADC_CHANNEL_1V1_BANDGAP    14
#define  ADC_CHANNEL_0V_GND         15

#endif /* defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) */

#if defined (__AVR_ATmega2560__)

// ADC Reference Voltages
#define ADC_REF_VOLTAGE_AREF 0
#define ADC_REF_VOLTAGE_AVCC 1
#define ADC_REF_VOLTAGE_1V1 2
#define ADC_REF_VOLTAGE_2V56 3

// ADC Channel
#define  ADC_CHANNEL_SINGLE_0       0
#define  ADC_CHANNEL_SINGLE_1       1
#define  ADC_CHANNEL_SINGLE_2       2
#define  ADC_CHANNEL_SINGLE_3       3
#define  ADC_CHANNEL_SINGLE_4       4
#define  ADC_CHANNEL_SINGLE_5       5
#define  ADC_CHANNEL_SINGLE_6       6
#define  ADC_CHANNEL_SINGLE_7       7
#define  ADC_CHANNEL_SINGLE_8       32
#define  ADC_CHANNEL_SINGLE_9       33
#define  ADC_CHANNEL_SINGLE_10      34
#define  ADC_CHANNEL_SINGLE_11      35
#define  ADC_CHANNEL_SINGLE_12      36
#define  ADC_CHANNEL_SINGLE_13      37
#define  ADC_CHANNEL_SINGLE_14      38
#define  ADC_CHANNEL_SINGLE_15      39
#define  ADC_CHANNEL_1V1_BANDGAP    30
#define  ADC_CHANNEL_0V_GND         31

#endif /* defined (__AVR_ATmega2560__) */

#if defined (__AVR_ATtiny13__)

// ADC Reference Voltages
#define ADC_REF_VOLTAGE_VCC 0
#define ADC_REF_VOLTAGE_1V1 1

// ADC Channel
#define  ADC_SINGLE_CHANNEL_0 0
#define  ADC_SINGLE_CHANNEL_1 1
#define  ADC_SINGLE_CHANNEL_2 2
#define  ADC_SINGLE_CHANNEL_3 3

#define  ADC_SINGLE_CHANNEL_PB5 0
#define  ADC_SINGLE_CHANNEL_PB2 1
#define  ADC_SINGLE_CHANNEL_PB4 2
#define  ADC_SINGLE_CHANNEL_PB3 3

#endif /* defined (__AVR_ATtiny13__) */

#endif /* ADC_H_ */
