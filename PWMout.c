/* Simple PWM output to reduce stepper power consumption
 * Author: lukjp
 */

#include <avr/io.h>
#include "PWMout.h"

void pwmOutInit(void) {
    PWM_OUT_DDR |= (1 << PWM_OUT_PIN_NR);
    OCR0A = PWM_OUT_TIMER_TOP;
    OCR0B = PWM_OUT_TIMER_COMP;
    // Fast PWM mode
    // prescaler 8
    // OCR0A as TOP
    // non-inverting PWM at OC0B
    TCCR0A = (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
    TCCR0B = (1 << WGM02) | (1 << CS01);
}
