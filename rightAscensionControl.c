/* rightAscensionControl:
 * Author: lukjp
 */

#include <avr/io.h>
#include "ADC.h"
#include "config.h"
#include "rightAscensionControl.h"
#include "SSSC.h"
#include "utils.h"

static uint16_t speedOverrideZeroPoint;
static uint8_t speedOverrideWasEnabled;

void rightAscensionControlInit(void) {
    // configure in- and outputs: enable pull-up resistors for all inputs
    TEST_SPEED_PORT |= (1 << TEST_SPEED_PIN_NR);
    DIRECTION_SWITCH_PORT |= (1 << DIRECTION_SWITCH_PIN_NR);
    FOLLOW_SWITCH_PORT |= (1 << FOLLOW_SWITCH_PIN_NR);
    MOON_SWITCH_PORT |= (1 << MOON_SWITCH_PIN_NR);
    OVERRIDE_ENABLE_PORT |= (1 << OVERRIDE_ENABLE_PIN_NR);
    ssscSetup();
    adcInit(ADC_PRESCALER_64, ADC_NOT_LEFT_ADJUSTED, ADC_REF_VOLTAGE_AVCC,
    ADC_AUTOTRIGGER_DISABLED, ADC_INTERRUPT_DISABLED);
    adcEnable();
    speedOverrideWasEnabled = 0;
}

void rightAscensionControlLoop(void) {
    int32_t speed = 0;
    if (!(FOLLOW_SWITCH_PIN & (1 << FOLLOW_SWITCH_PIN_NR))) {
        if (!(MOON_SWITCH_PIN & (1 << MOON_SWITCH_PIN_NR))) {
            speed = MOON_STEPPER_SPEED;
        } else {
            speed = STARS_STEPPER_SPEED;
        }
    }

    if (!(OVERRIDE_ENABLE_PIN & (1 << OVERRIDE_ENABLE_PIN_NR))) {
        if (speedOverrideWasEnabled) {
            uint16_t ADCResult;
            adcMeasure10BitNSum(&ADCResult, 4);
            int16_t controldifference = ((int16_t) ADCResult)
                    - speedOverrideZeroPoint;
            // 1024 * 4 because we use the sum of 4 measurements with maximum of 1023
            int32_t speeddifference = DIV_ROUND_S_U(
                    (((int64_t) controldifference) * OVERRIDE_MAX_SPEED),
                    (1024 * 4));
            speed = addSatInt32(speed, speeddifference);
        } else {
            speedOverrideWasEnabled = 1;
            adcMeasure10BitNSum(&speedOverrideZeroPoint, 16);
            // divide by 4 because we used the sum of 16 values instead of 4
            speedOverrideZeroPoint = DIV_ROUND_U_U(speedOverrideZeroPoint,
                    4);
        }
    } else {
        speedOverrideWasEnabled = 0;
    }
    if (!(TEST_SPEED_PIN & (1 << TEST_SPEED_PIN_NR))) {
        speed = STEPPER_MAX_SPEED;
    }
    if (!(DIRECTION_SWITCH_PIN & (1 << DIRECTION_SWITCH_PIN_NR))) {
        ssscSetSpeed(-speed);
    } else {
        ssscSetSpeed(speed);
    }
}
