/*
 * Small library for Handling the ADC of some AVR microcontrollers
 * See ADC.h for more info
 * Author: lukjp
 */

#include <avr/io.h>
#include <stdint.h>
#include "ADC.h"

// general part, almost the same or all supported devices
#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) || (__AVR_ATmega2560__) || defined (__AVR_ATtiny13__)

uint8_t adcInit(uint8_t prescaler, uint8_t eightBitMode, uint8_t refVoltage,
        uint8_t autotrigger, uint8_t interruptEnable) {
    if ((prescaler > 7) || (eightBitMode > 1) || (refVoltage > 3)
            || (autotrigger > 1) || (interruptEnable > 1)) {
        return 1;
    }
    ADMUX = ((refVoltage << REFS0) | (eightBitMode << ADLAR)) | (ADMUX & 0x0F);
    ADCSRA = (ADCSRA & ((1 << ADSC) | (1 << ADIF))) | (1 << ADEN)
            | (autotrigger << ADATE) | (interruptEnable << ADIE)
            | (prescaler << ADPS0);
    return 0;
}

uint8_t adcSetAutoTriggerSource(uint8_t source) {
#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__) || defined (__AVR_ATmega2560__)
    if (source > 7) {
        return 1;
    }
#endif
#if defined (__AVR_ATtiny13__)
    if (source > 6) {
        return 1;
    }
#endif
    ADCSRB = (ADCSRB & 0xF8) | source;
    return 0;
}

uint8_t adcMeasure8Bit(uint8_t *result) {
    if (adcStartConversion()) {
        return 1;
    }
    while (adcInUse()) {

    }
    *result = adcRead8Bit();
    return 0;
}

uint8_t adcMeasure10Bit(uint16_t *result) {
    if (adcStartConversion()) {
        return 1;
    }
    while (adcInUse()) {

    }
    *result = adcRead10Bit();
    return 0;
}

uint8_t adcMeasure10BitNSum(uint16_t *result, uint8_t n) {
    if (n > 64) {
        return 1;
    }
    uint16_t tmpResult = 0;
    for (int i = 0; i < n; i++) {
        uint16_t singleResult;
        while (adcMeasure10Bit(&singleResult)) {

        }
        tmpResult += singleResult;
    }
    *result = tmpResult;
    return 0;
}

uint8_t adcStartConversion(void) {
    if (adcInUse()) {
        return 1;
    }
    ADCSRA |= (1 << ADSC);
    return 0;
}

uint8_t adcRead8Bit() {
    return ADCH;
}

uint16_t adcRead10Bit() {
    return ADCW;
}

uint8_t adcInUse() {
    if (ADCSRA & (1 << ADSC)) {
        return 1;
    }
    return 0;
}

void adcEnable() {
    ADCSRA |= (1 << ADEN);
}

void adcDisable() {
    ADCSRA &= ~(1 << ADEN);
}

#endif

#if defined (__AVR_ATmega328P__) || defined (__AVR_ATmega328__)

uint8_t adcSetChannel(uint8_t channel) {
    if (channel > 8 && (channel != 14) && (channel != 15))
        return 1;
    ADMUX = (ADMUX & 0xF0) | (channel & 0x0F);
    return 0;
}

#endif

#if defined (__AVR_ATmega2560__)

uint8_t adcSetChannel(uint8_t channel) {
    if (channel > 0x3D) {
        return 1;
    }
    ADMUX = (ADMUX & 0x80) | (channel & 0x1F);
    ADCSRB = (ADCSRB & ~(1 << MUX5) ) | ( (channel >> 2) & (1 << MUX5) );
    return 0;
}

#endif

#if defined (__AVR_ATtiny13__)

uint8_t adcSetChannel(uint8_t channel) {
    if (channel > 3) {
        return 1;
    }
    ADMUX = (ADMUX & 0xFC) | channel;
    return 0;
}

#endif
