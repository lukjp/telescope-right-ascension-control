/*
 * Small and simple library for handling an unipolar stepper motor.
 * Author: lukjp
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <util/atomic.h>
#include "stepper.h"
#include "utils.h"

static uint32_t speedToTicsPerStep(int32_t stepTime);
static uint32_t ticsPerStepToSpeed(uint32_t ticsPerStep);

uint8_t stepperCreateConfig(struct StepperConfig *config, uint32_t maxSpeed,
        uint32_t maxAcceleration, uint32_t recalculationInterval) {
    uint8_t error = 0;
    // *1000 because maxSpeedDifference is in steps per 1000 seconds
    // /F_CPU because recalcTime is in ticks
    uint64_t maxSpeedDifference = DIV_ROUND_U_U(
            (((uint64_t ) maxAcceleration) * recalculationInterval * 1000),
            F_CPU);
    if (maxSpeedDifference > INT32_MAX) {
        error = 1;
        maxSpeedDifference = INT32_MAX;
    }
    config->maxSpeedDifference = maxSpeedDifference;

    if (maxSpeed > INT32_MAX) {
        error = 1;
        maxSpeed = INT32_MAX;
    }
    config->maxSpeed = maxSpeed;
    return error;
}

uint8_t stepperInit(struct StepperConfig *config, struct Stepper *stepper,
        volatile uint8_t * port, uint8_t usedBitMask, uint8_t offBitMask,
        uint8_t stateCount, const uint8_t * stateBitMask) {
    stepper->config = config;
    stepper->port = port;
    stepper->usedBitMask = usedBitMask;
    stepper->stateCount = stateCount;
    stepper->stateBitMask = stateBitMask;
    stepper->offBitMask = offBitMask;
    stepper->currentSpeed = 0;
    stepper->setTicsPerStep = 0;
    stepper->setSpeed = 0;
    stepper->state = 0;
    stepper->mode = disabled;
    uint8_t error = 0;
    // check if stateBitMask is valid
    for (uint8_t i = 0; i < stateCount; i++) {
        if (stateBitMask[i] & ((uint8_t) ~usedBitMask)) {
            error = 1;
        }
    }
    // check if offBitMask is valid
    if (offBitMask & ((uint8_t) ~usedBitMask)) {
        error = 1;
    }
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        *(stepper->port) = (*(stepper->port) & (~(stepper->usedBitMask)))
                | stepper->offBitMask;
    }
    return error;
}

uint8_t stepperDisable(struct Stepper *stepper) {
    uint8_t error;
    switch (stepper->mode) {
    case disabled:
        error = 1;
        break;
    default: // running
        error = 0;
        stepper->mode = disabled;
        break;
    }
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        *(stepper->port) = (*(stepper->port) & (~(stepper->usedBitMask)))
                | stepper->offBitMask;
    }
    return error;
}

uint8_t stepperStart(struct Stepper *stepper) {
    uint8_t error;
    switch (stepper->mode) {
    case running:
        case accelerating:
        error = 1;
        break;
    case disabled:
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
        {
            stepper->mode = accelerating;
            stepper->currentSpeed = 0;
        }
        error = 0;
        break;
    }
    return error;
}

uint8_t stepperSetSpeed(struct Stepper *stepper, int32_t speed) {
    uint8_t error = 0;
    error = clip32Bit(&speed, stepper->config->maxSpeed);
    uint32_t setTicsPerStep = speedToTicsPerStep(speed);
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        stepper->setSpeed = speed;
        stepper->setTicsPerStep = setTicsPerStep;
        if (stepper->mode == running) {
            stepper->mode = accelerating;
        }
    }
    return error;
}

uint8_t stepperSetTicsPerStep(struct Stepper *stepper, uint32_t ticsPerStep,
        uint8_t backwards) {
    uint8_t error = 0;
    uint32_t absoluteSpeed = ticsPerStepToSpeed(ticsPerStep);
    if (absoluteSpeed > stepper->config->maxSpeed) {
        absoluteSpeed = stepper->config->maxSpeed;
        error = 1;
    }
    if (error) {
        ticsPerStep = speedToTicsPerStep(absoluteSpeed);
    }
    int32_t speed = absoluteSpeed;
    if (backwards) {
        speed *= -1;
    }
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        stepper->setSpeed = speed;
        stepper->setTicsPerStep = ticsPerStep;
        if (stepper->mode == running) {
            stepper->mode = accelerating;
        }
    }
    return error;
}

struct StepperStepTime stepperRecalc(struct Stepper *stepper) {
    struct StepperStepTime stepperStepTime;
    switch (stepper->mode) {
    case accelerating:
        ;
        //create local copies because of volatile
        int32_t setSpeed = stepper->setSpeed;
        int32_t currentSpeed = stepper->currentSpeed;
        if (setSpeed == currentSpeed) {
            // switch from set speed to set step time
            stepperStepTime.stepTimeMode = updateStepTime;
            stepperStepTime.stepTime = stepper->setTicsPerStep;
            stepper->mode = running;
        } else {
            // calculate new speed
            int32_t newSpeed = nearVal(currentSpeed, setSpeed,
                    stepper->config->maxSpeedDifference);
            // write new speed (atomic because Step function reads it)
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
            {
                stepper->currentSpeed = newSpeed;
            }
            // calculate step time
            // for this we are only interested in the absolute vale for
            // calculating the step time
            if (newSpeed == 0) {
                stepperStepTime.stepTimeMode = noStep;
            } else {
                stepperStepTime.stepTime = speedToTicsPerStep(newSpeed);
                stepperStepTime.stepTimeMode = updateStepTime;
            }
        }
        break;
    case running:
        // We do not need to to anything
        stepperStepTime.stepTimeMode = keepStepTime;
        stepperStepTime.stepTime = stepper->setTicsPerStep;
        break;
    case disabled:
        stepperStepTime.stepTimeMode = noStep;
        break;
    }
    return stepperStepTime;
}

void stepperStep(struct Stepper *stepper) {
    switch (stepper->mode) {
    case disabled:
        break;
    case running:
        case accelerating:
        ;
        int32_t currentSpee = stepper->currentSpeed;
        if (currentSpee != 0) {
            uint8_t state = stepper->state;
            if (currentSpee >= 0) {
                state = (state + 1) % stepper->stateCount;
            } else {
                // we need this cast, else we would get from 0 to 255
                state = ((uint8_t) (state - 1)) % stepper->stateCount;
            }
            // Change port atomically to do not overwrite a possible other
            // change made between read and write of the port since the line
            // needs more than one instruction.
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
            {
                *(stepper->port) =
                        (*(stepper->port) & (~(stepper->usedBitMask)))
                                | stepper->stateBitMask[state];
            }
            stepper->state = state;
        }
        break;
    }
    return;
}

static uint32_t speedToTicsPerStep(int32_t speed) {
    if (speed == 0) {
        return 0;
    }
    uint32_t absoluteSpeed = (speed < 0 ? -speed : speed);
    // if F_CPU is to large, we split the multiplication to avoid a 64 bit
    // division. 100000000 (1E8) is the maximum expected speed for the stepper
    // (100k steps per second)
#if (F_CPU * 1000 + 100000000) > INT32_MAX
    return DIV_ROUND_U_U(F_CPU * 125, absoluteSpeed) * 8;
#else
    return DIV_ROUND_U_U(F_CPU * 1000, absSpeed);
#endif
}

static uint32_t ticsPerStepToSpeed(uint32_t ticsPerStep) {
    if (ticsPerStep == 0) {
        return 0;
    }
    // Unlike speedToTicsPerStep() this function is only called when setting
    // the speed, since this is not time critical, a 64 bit integer division is
    // acceptable.
    return (uint32_t) DIV_ROUND_U_U((uint64_t) F_CPU * 1000, ticsPerStep);
}
