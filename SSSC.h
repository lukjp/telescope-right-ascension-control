/* SSSC: Simple Single Stepper Controller
 * Very simple to use controller for a stepper, works on ATMega8, ATMega328 and
 * ATMega328p.
 * It uses Timer0 for acceleration control and Timer1 as step timer.
 * Usage:
 *  - call SSSC_setup() to set up stepper
 *  - call SSSC_setSpeed(int32_t speed) to set the speed of the motor
 * 
 * TODO:
 *  - implement switching of step timer prescaler for greater resolution and
 *      range of the step time
 *  - enable stepping interrupt during step time recalculation to reduce jitter
 * 
 * Author: lukjp
 */

#include <stdint.h>

#ifndef SSSC_H_
#define SSSC_H_

// NOTE: The following constants can be changed

// port where the stepper is connected
#define STEPPER_PORT PORTD
// data directory register for the port where the stepper is connected
#define STEPPER_DDR DDRD
// pin numbers of the stepper coils
#define STEPPER_PIN_0 0
#define STEPPER_PIN_1 1
#define STEPPER_PIN_2 2
#define STEPPER_PIN_3 3

// automatically disable the stepper if the set speed is 0
// 0 for no auto disabling, 1 for auto disabling
#define STEPPER_AUTO_DISABLE 1
// use half steps for the stepper
// 0 for no half steps, 1 for half steps
#define STEPPER_USE_HALF_STEPS 1
// maximum speed of the stepper in steps per 1000 seconds
#define STEPPER_MAX_SPEED 1000000UL
// maximum acceleration of the stepper in (steps per second) per second
#define STEPPER_MAX_ACCEL 500UL

// NOTE: The following constants should not be changed

#define RECALC_TIMER_PRESCALER 256UL
#define RECALC_TIMER_TOP 255UL

#define RECALC_TIME (RECALC_TIMER_PRESCALER * (RECALC_TIMER_TOP + 1))

// mask of used bits for stepper control, 1 for bits used by the stepper, 0 for unrelated bits
#define STEPPER_USED_BIT_MASK ((1 << STEPPER_PIN_0) | (1 << STEPPER_PIN_1) | (1 << STEPPER_PIN_2) | (1 << STEPPER_PIN_3))
#define STEPPER_OFF_BIT_MASK 0
// step count until the step pattern repeats
#if (STEPPER_USE_HALF_STEPS)
#define STEPPER_STATE_COUNT 8
#else
#define STEPPER_STATE_COUNT 4
#endif

/* setup stepper with timing
 */
void ssscSetup(void);

/* Set  speed of the stepper motor.
 * Parameters:
 *  - int32_t speed : new speed for the motor in steps per 1000 seconds
 */
void ssscSetSpeed(int32_t speed);

/* Set  speed of the stepper motor.
 * Parameters:
 *  - ticsPerStep: new speed for the motor in tics per Step for the stepper
 *  - backwards: non-zero to set motor running backwards.
 */
void ssscSetTicsPerStep(uint32_t ticsPerStep, uint8_t backwards);

#endif // SSSC_H_
