# telescope-right-ascension-control

Simple control of the right ascension of a telescope with an Atmel AVR microcontroller and a stepper motor.

## Architecture
main.c handles the control input and sets the stepper speed. For reading the override potentiometer, it uses ADC.c and ADC.h which are targeted for some AVR microcontrollers. Stepper control is handled on low level by stepper.c which should work on any AVR. Between main and stepper.c is SSSC.c, which provides a simple interface to control the speed of one stepper on an ATMega 8, ATMega 328 or ATMega 328p. Unlike stepper.c it is configured by SSSC.h via precompiler macros.

## Features
- compile time settable speed for moon and stars
- runtime switch between moon and stars speed
- runtime direction switch
- compile time setting to enable half steps
- override the speed with a potentiometer
- override works in addition to tracking speed, relative to the target
- compile time switch to automatically turn off stepper if it is standing still

## User configuration

### SSSC.h
- maximum speed for the stepper
- maximum acceleration for the stepper
- pins where the stepper is connected
- usage of half steps (note: changing needs change of STEPPER_STEP_COUNT in config.h)

### config.h
- input pins
- gear transmission ratio between stepper and telescope mount
- Step count of the stepper for one revolution

## Control
- _direction_ normal / inverted switch: switch direction of the output stepper
- _follow_ on / off switch: turn right ascension with the programmed speed
- _speed_ moon / stars switch: use the angular speed of the moon or of the stars
- _override enable_ on / off switch: enable override control via potentiometer.
- _override_ potentiometer: control the speed of the stepper manually, the control value is added to the follow speed.
    The potentiometer is calibrated each time _override enable_ is turned on.
- _test speed_ on / off switch: run the motor with 1 rev/s to check if step count is correct

## Notes
- currently all inputs are normally high to make use of the internal pull-up resistors, which are enabled

## Future features

### Better manual control
- _dead zone_: minimal input does nothing to work around change of potentiometer rest position
- nonlinear override: fine control at slow speeds and fast maximum speed

### Battery watch
- check battary voltage regularly and turn stepper off if too low

### Out of source build
- seperate build and source files (help with MAKEFILE wanted)

## Possible extended features

### Program mode
- either as compile time option or as switch
- used to "train" the system to the desired speed
- saves a correction factor in the EEPROM

### Test photo mode
- rotate with maximum speed for some time
- if a picture is shot during that time and the right ascension axis is correctly set up, every star except Polaris should show trails.
